import org.bitbucket.alexmelyon.kserver.KChat
import org.bitbucket.alexmelyon.kserver.KClient
import org.bitbucket.alexmelyon.kserver.KClientListener
import org.testng.Assert
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test

class KChatTest {


    lateinit var kchat: KChat

    @BeforeMethod
    fun setUp() {
        kchat = KChat()
    }

    @Test
    fun test1000Connections() {

        val listener = TestClient()
        // Wait for nickname prompt
        Thread.sleep(100)
        listener.messages.clear()

        val clients = (1..1000).map { KClient() }
        clients.forEach {
            it.connect()
        }
        clients.forEachIndexed { index, kClient ->
            kClient.send(index.toString())
            kClient.send(index.toString())
            Thread.sleep(60)
        }
        Assert.assertEquals(1000, listener.messages.size)
    }

    class TestClient : KClientListener {

        val messages = mutableListOf<String>()

        init {
            val kClient = KClient()
            kClient.listeners.add(this)
            kClient.connect()
        }

        override fun onOpen() {}

        override fun onMessage(message: String) {
            messages.add(message)
        }

    }
}