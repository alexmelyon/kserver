package org.bitbucket.alexmelyon.kserver

import java.net.Socket

class KChat : KServerListener {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            KChat()
        }
    }

    val kServer = KServer()
    val clients = mutableMapOf<Socket, String?>()
    val history = mutableListOf<String>()

    init {
        kServer.listeners.add(this)
        kServer.start()
    }

    override fun onStart() {
        println("Started")
    }

    override fun onOpen(socket: Socket) {
        clients.put(socket, null)
        kServer.broadcast("Please enter your name:", listOf(socket))
    }

    override fun onMessage(socket: Socket, message: String) {
        val nick = clients[socket]
        if (nick == null) {
            clients[socket] = message
            history.forEach { kServer.broadcast(it, listOf(socket)) }
            kServer.broadcast("Your nickname is $message", listOf(socket))
        } else {
            val line = "$nick: $message"
            updateHistory(line)
            kServer.broadcast(line)
        }
    }

    fun updateHistory(message: String) {
        history.add(message)
        val count = history.size
        if (count > 100) {
            history.drop(count - 100)
        }
    }

    override fun onError(ex: Exception) {
        ex.printStackTrace()
    }

    override fun onClose(socket: Socket) {
        val nick = clients[socket]!!
        println("$nick leave")
        kServer.broadcast("$nick leave")
        clients.remove(socket)
    }
}