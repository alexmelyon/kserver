package org.bitbucket.alexmelyon.kserver

import java.net.ServerSocket
import java.net.Socket
import java.nio.charset.StandardCharsets
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class KServer(val port: Int = 8080) {

    val listeners = mutableListOf<KServerListener>()

    private val clientSockets = mutableListOf<Socket>()
    private lateinit var server: ServerSocket
    private val threadPool = Executors.newCachedThreadPool()

    fun getClients(): List<Socket> {
        return clientSockets
    }

    fun start(): Thread {
        try {
            server = ServerSocket(port)
        } catch (ex: Exception) {
            listeners.forEach { it.onError(ex) }
            return Thread {}
        }
        listeners.forEach { it.onStart() }
        startConnectionChecker()
        val serverThread = Thread {
            server.use {
                while (true) {
                    val socket = server.accept()
                    clientSockets.add(socket)
                    threadPool.submit {
                        listeners.forEach { it.onOpen(socket) }

                        val reader = socket.getInputStream()
                                .bufferedReader(StandardCharsets.UTF_8)

                        do {
                            val line = reader.readLine()
                            if (line != null) {
                                listeners.forEach { it.onMessage(socket, line) }
                            }
                        } while (line != null)

                        clientSockets.remove(socket)
                        listeners.forEach { it.onClose(socket) }
                    }
                }
            }
        }
        serverThread.start()
        return serverThread
    }

    fun broadcast(message: String, clients: List<Socket> = clientSockets) {
        for (client in clients) {
            val writer = client.getOutputStream().bufferedWriter(StandardCharsets.UTF_8)
            writer.write(message)
            writer.newLine()
            writer.flush()
        }
    }

    private fun startConnectionChecker() {
        val clients = mutableListOf<Socket>()
        val runnable = Runnable {
            val terminated = mutableListOf<Socket>()
            for (client in clients) {
                try {
                    client.getOutputStream().write(' '.toInt())
                    client.getOutputStream().flush()
                } catch (t: Throwable) {
                    terminated.add(client)
                }
            }
            terminated.forEach { client ->
                listeners.forEach { it.onClose(client) }
                clients.remove(client)
            }
        }
        val connectionChecker = Executors.newScheduledThreadPool(1)
        connectionChecker.scheduleAtFixedRate(runnable, 0L, 10L, TimeUnit.SECONDS)
    }
}