package org.bitbucket.alexmelyon.kserver

import java.net.Socket

interface KServerListener {
    fun onStart()
    fun onOpen(socket: Socket)
    fun onMessage(socket: Socket, message: String)
    fun onError(ex: Exception)
    fun onClose(socket: Socket)
}