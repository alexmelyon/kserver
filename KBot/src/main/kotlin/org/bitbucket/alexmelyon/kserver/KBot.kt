package org.bitbucket.alexmelyon.kserver

import java.io.BufferedReader
import java.io.InputStreamReader


class KBot : KClientListener {

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            KBot()
        }
    }

    val kClient = KClient()
    // 99 bottles
    val index = mutableMapOf<String, Int>()
    val bottles: List<List<String>>

    init {
        kClient.listeners += this
        kClient.connect()
        //
        val resource = javaClass.getResourceAsStream("/bottles.txt")
        val reader = BufferedReader(InputStreamReader(resource))
        val text = reader.readLines().joinToString("\n")
        bottles = text.split("\n\n").map { it.split("\n") }
    }

    override fun onOpen() {
        kClient.send("KBot")
    }

    override fun onMessage(message: String) {
        val nick = message.substringBefore(":").trim()
        val command = message.substringAfter(":").trim()
        when (command) {
            "/99" -> bottles(nick)
        }
    }

    fun bottles(nick: String) {
        val i = index[nick] ?: 0
        val lines = bottles[i]
        lines.forEach {
            kClient.send(it)
        }
        index[nick] = (i + 1) % bottles.size
    }
}