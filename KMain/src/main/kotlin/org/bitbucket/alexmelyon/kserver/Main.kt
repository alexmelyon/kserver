package org.bitbucket.alexmelyon.kserver

fun main(args: Array<String>) {
    KChat()
    KBot()
    KClient().apply {
        listeners.add(object : KClientListener {
            override fun onOpen() {
                println("Opened")
            }
            override fun onMessage(message: String) {
                println(message)
            }
        })
        connect()
        listen(System.`in`)
    }
}