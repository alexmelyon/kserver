package org.bitbucket.alexmelyon.kserver

interface KClientListener {
    fun onOpen()
    fun onMessage(message: String)
}