package org.bitbucket.alexmelyon.kserver

import java.io.InputStream
import java.net.Socket
import java.nio.charset.StandardCharsets
import java.util.*

class KClient(val host: String = "localhost", val port: Int = 8080) {
    val listeners = mutableListOf<KClientListener>()
    private lateinit var socket: Socket
    fun connect(): Thread {
        socket = Socket(host, port)
        listeners.forEach { it.onOpen() }
        val readerThread = Thread {
            while (true) {
                val reader = socket.getInputStream().bufferedReader(StandardCharsets.UTF_8)
                val input = reader.readLine().trim()
                if (input.isNotBlank()) {
                    listeners.forEach { it.onMessage(input) }
                }
            }
        }
        readerThread.start()
        return readerThread
    }

    fun send(message: String) {
        if (message == "exit") {
            socket.getOutputStream().close()
        }
        val writer = socket.getOutputStream().bufferedWriter(StandardCharsets.UTF_8)
        writer.write(message)
        writer.newLine()
        writer.flush()
    }

    fun listen(input: InputStream) {
        val scanner = Scanner(input)
        while (true) {
            val line = scanner.next()
            if(line.isNotBlank()) {
                send(line)
            }
        }
    }
}

fun main(args: Array<String>) {
    val host = args.firstOrNull() ?: "localhost"
    val port = args.getOrNull(1)?.toInt() ?: 8080

    val client = KClient(host, port)
    client.listeners += object : KClientListener {
        override fun onOpen() {
            println("Opened")
        }

        override fun onMessage(message: String) {
            println(message)
        }

    }
    client.connect()
    client.listen(System.`in`)
}